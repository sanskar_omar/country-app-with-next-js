import React, { useState, useEffect } from 'react'
import Layout from '../../components/Layout'
import Head from "next/head"

// Alpha3code is not avaliable in API fetched data
// So i am unable to verify if there are any mistakes in this page
// API docs page is down hence API endpoints not avaliable

export default function Country() {
    return (
        <Layout>
            <Head>
                <title>[CountryName]</title>
            </Head>

            {/* Placeholder Card */}
            <div>
                <h3 className="mt-4 mb-4"><center>alpha3code Not avaliable in API Response</center></h3>
                <div className="card bg-secondary mb-3 shadow-lg" aria-hidden="true">
                    <div className="row g-0">
                        <div className="col-md-4">
                            <img src="" className="img-fluid rounded-start" alt="" />
                        </div>
                        <div className="col-md-8 card-body">
                            <h5 className="card-title placeholder-glow">
                                <span className="placeholder col-6"></span>
                            </h5>
                            <p class="card-text placeholder-glow">
                                <span className="placeholder col-5"></span>
                                <br />
                                <span className="placeholder col-4"></span>
                                <span className="placeholder col-3"></span>
                                <span className="placeholder col-3"></span>
                            </p>
                            <button
                                type="button"
                                className="btn btn-info disabled placeholder col-3"
                            ></button>
                            <button
                                type="button"
                                className="btn btn-info disabled placeholder col-2"
                            ></button>
                        </div>
                    </div>
                </div>
            </div>

        </Layout>

    )
}