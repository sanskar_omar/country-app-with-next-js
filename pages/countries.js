import React, { useState } from 'react'
import Layout from '../components/Layout'
import CountryCard from '../components/CountryCard.js'

export default function AllCountries({ countries }) {

    const [keyword, setKeyword] = useState("");
    // Filter according to search-bar text
    const filterCountries = countries.filter(country =>
        country.name.common.toLowerCase().includes(keyword));

    // Track input change of searchbar
    const onInputChange = (e) => {
        e.preventDefault();
        setKeyword(e.target.value.toLowerCase());
    }

    return (
        <Layout>
            <div className="container justify-content-center col-lg-8 py-4">
                <h3 className="display-6"> Countries </h3>
            </div>
            <form className="form my-2 my-lg-0 col-sm-6 d-flex flex-row m-auto">
                <input
                    className="form-control mr-sm-2"
                    type="search"
                    placeholder="Search Countries"
                    aria-label="Search"
                    id="search_input"
                    onChange={onInputChange}
                />
            </form>
            <br />
            <CountryCard countries={filterCountries} />
        </Layout>
    )
}

export const getStaticProps = async () => {
    const res = await fetch("https://restcountries.com/v3.1/all");
    const data = await res.json();

    // Sorting alphabetically by country's common name
    let countries = data.sort(function (a, b) {
        return a.name.common.localeCompare(b.name.common);
    });
    return {
        props: {
            countries
        }
    }
}