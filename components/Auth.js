import React, { useState } from "react"
import { useForm } from "react-hook-form"
import Link from 'next/link'
import Layout from './Layout'
import style from '../styles/auth.module.css'

export default function App() {
    const [formType, setFormType] = useState('login');
    const { register, formState: { errors }, handleSubmit } = useForm();
    const onSubmit = data => console.log(data);

    return (
        <Layout>
            <div className="py-4">
                <h3>Country App </h3>
            </div>
            <div className={style.authForm}>
                <div className="d-flex flex-row btn-info">
                    <button className="btn btn-info w-50" onClick={() => setFormType('signup')}>Sign Up</button>
                    <button className="btn btn-info w-50" onClick={() => setFormType('login')}>Log In</button>
                </div>


                {/* To switch between login and signup forms based on values of formtype in useState hook*/}
                {(formType === 'signup') ?
                    <form onSubmit={handleSubmit(onSubmit)} className="p-4 rounded-bottom bg-dark text-white">
                        <div className="form-group">
                            <label>First Name</label>
                            <input {...register("firstName", { required: true, maxLength: 20 })} className="form-control" placeholder="First Name" />
                            {errors.firstName?.type === 'required' && "First name is required"}
                        </div>
                        <div className="form-group">
                            <label>Last Name</label>
                            <input {...register("lastName", { pattern: /^[A-Za-z]+$/i })} className="form-control" placeholder="Last Name" />
                        </div>
                        <div className="d-flex flex-row">
                            <div className="form-group w-50">
                                <label>Age</label>
                                <input type="number" {...register("age", { min: 0, max: 99 })} placeholder="Select Age" className="form-control" />
                            </div>
                            <div className="form-group w-50">
                                <label>Gender</label>
                                <select {...register("gender")} className="form-control">
                                    <option value="null">--Select--</option>
                                    <option value="female">female</option>
                                    <option value="male">male</option>
                                    <option value="other">other</option>
                                </select>
                            </div>
                        </div>
                        <div className="form-group">
                            <label>Email</label>
                            <input type="email" {...register("email", { required: true })} className="form-control" aria-describedby="emailHelp" placeholder="Enter email" />
                            {errors.email?.type === 'required' && "Email is required"}
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <input type="password" {...register("password", { required: true })} className="form-control" placeholder="Password" />
                            <small className="form-text text-muted">We suggest to choose a strong password.</small>
                        </div>

                        <Link href="/countries"><a><input type="submit" className="btn btn-info m-auto" value="Sign Up" /></a></Link>

                    </form>
                    :
                    <form onSubmit={handleSubmit(onSubmit)} className="p-4 rounded-bottom bg-dark text-white">
                        <div className="form-group">
                            <label>Email</label>
                            <input type="email" {...register("email", { required: true })} className="form-control" aria-describedby="emailHelp" placeholder="Enter email" />
                            {errors.email?.type === 'required' && "Email is required"}
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <input type="password" {...register("password", { required: true })} className="form-control" placeholder="Password" />
                            <small className="form-text text-muted">Forgot Password?</small>
                        </div>

                        <Link href="/countries"><a><input type="submit" className="btn btn-info m-auto" value="Log In" /></a></Link>

                    </form>}


            </div>
        </Layout>

    );
}