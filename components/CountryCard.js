import React from 'react'
import Link from 'next/link'

export default function CountryCard({ countries }) {
    return (
        <div>
            {countries.map((country) => (
                <div className="card text-white bg-dark mb-3 shadow-lg">
                    <div className="row g-0">
                        <div className="col-md-4">
                            <img
                                src={country.flags.svg}
                                className="img-fluid rounded-start"
                                alt={country.name.common}
                            />
                        </div>
                        <div className="col-md-8 card-body">
                            <h5 className="card-title">{country.name.common}</h5>
                            <p className="card-text">
                                Capital: {country.capital}<br />Current Date and Time: {country.timezones}
                            </p>
                            <Link href={`https://maps.google.com/?q=${country.latlng}`}><a className="p-1" target="_blank"><button type="button" className="btn btn-info">Show Map</button></a></Link>
                            <Link href={`/country/${country.name.common}`}><a className="btn btn-info" type="button">Details</a></Link>
                        </div >
                    </div >
                </div >
            ))
            }
        </div>
    )
}